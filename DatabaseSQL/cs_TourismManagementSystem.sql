-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 15, 2020 at 02:08 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1-log
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs_TourismManagementSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `Customers`
--

CREATE TABLE `Customers` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Address` varchar(60) NOT NULL,
  `City` varchar(15) NOT NULL,
  `State` varchar(10) NOT NULL,
  `Country` varchar(15) NOT NULL,
  `PostCode` varchar(10) NOT NULL,
  `Phone` varchar(24) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `RegesterDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Customers`
--

INSERT INTO `Customers` (`Id`, `Name`, `Address`, `City`, `State`, `Country`, `PostCode`, `Phone`, `Email`, `RegesterDate`) VALUES
(3, 'Kevin', '5075 walkley', 'Montreal', 'BC', 'Canada', 'H9R4Y5', '5145414454', 'kevin@gmail.com', '2020-03-08'),
(5, 'Shuixiu', '400 hermitage', 'Montreal', 'QC', 'Canada', 'H4V2M2', '514561024', 'shuixiutan@gmail.com', '2020-03-26'),
(7, 'Meng', '15 larong', 'Montreal', 'QC', 'Canada', 'h4y3y3', '1541452122', 'mengmeng@gmail.com', '2020-04-08');

-- --------------------------------------------------------

--
-- Table structure for table `Employees`
--

CREATE TABLE `Employees` (
  `Id` int(11) NOT NULL,
  `EName` varchar(50) NOT NULL,
  `Address` varchar(60) NOT NULL,
  `City` varchar(15) NOT NULL,
  `State` varchar(15) NOT NULL,
  `Country` varchar(15) NOT NULL,
  `PostCode` varchar(10) NOT NULL,
  `Phone` varchar(25) NOT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `HireDate` date NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `RoleLevel` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Employees`
--

INSERT INTO `Employees` (`Id`, `EName`, `Address`, `City`, `State`, `Country`, `PostCode`, `Phone`, `Email`, `HireDate`, `Username`, `Password`, `RoleLevel`) VALUES
(1, 'Admin', '333 hermitage', 'Pointe Claire', 'QC', 'Canada', 'h9r4y3', '51423521545', 'shanyao@gmail.com', '2020-03-10', 'admin', '21232F297A57A5A743894AE4A801FC3', 'Admin'),
(3, 'ShuiXiu', '438 hermitage', 'Pointe Claire', 'QC', 'Canada', 'h4y4y3', '4383453986', 'shuixiutan@gmail.com', '2020-03-11', 'shuixiutan', 'E1ADC3949BA59ABBE56E057F2F883E', 'Employee'),
(4, 'Maggie', '5870 walkley', 'Montreal', 'QC', 'Canada', 'H9r3y4', '42156424521', 'maggiejiang@gmail.com', '2020-04-02', 'maggiejiang', '123456', 'Employee'),
(5, 'ShengyongJiang', '15 laronde', 'Montreal', 'QC', 'Canada', 'h9r3ye', '4351542545', 'shengyongjiang@gmail.com', '2020-04-07', 'shengyong', '123456', 'System.Win');

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `Id` int(11) NOT NULL,
  `TourGroupId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `NumberOfPeople` int(11) NOT NULL,
  `OrderDate` date NOT NULL,
  `Status` int(11) NOT NULL,
  `Description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Orders`
--

INSERT INTO `Orders` (`Id`, `TourGroupId`, `CustomerId`, `NumberOfPeople`, `OrderDate`, `Status`, `Description`) VALUES
(16, 2, 3, 3, '2020-04-08', 1, '3 pp'),
(19, 6, 7, 5, '2020-04-12', 1, '5 pp'),
(27, 5, 3, 1, '2020-04-12', 1, ''),
(28, 5, 3, 31, '2020-04-12', 1, '22'),
(29, 6, 3, 4, '2020-04-13', 1, '2 KIS, 2 Alduts'),
(30, 14, 3, 4, '2020-04-13', 1, '2 kids, 2 adults'),
(31, 2, 3, 5, '2020-04-13', 1, '3 kids,2 asult'),
(32, 5, 3, 3, '2020-04-06', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `TaskLogs`
--

CREATE TABLE `TaskLogs` (
  `Id` int(11) NOT NULL,
  `Task` varchar(50) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TaskLogs`
--

INSERT INTO `TaskLogs` (`Id`, `Task`, `Date`) VALUES
(1, 'FirstLogin', '2020-04-11'),
(2, 'FirstLogin', '2020-04-11'),
(3, 'FirstLogin', '2020-04-11'),
(4, 'FirstLogin', '2020-04-12'),
(5, 'FirstLogin', '2020-04-12'),
(6, 'FirstLogin', '2020-04-12'),
(7, 'FirstLogin', '2020-04-12'),
(8, 'FirstLogin', '2020-04-12'),
(9, 'FirstLogin', '2020-04-12'),
(10, 'FirstLogin', '2020-04-12'),
(11, 'FirstLogin', '2020-04-12'),
(12, 'FirstLogin', '2020-04-12'),
(13, 'FirstLogin', '2020-04-12'),
(14, 'FirstLogin', '2020-04-12'),
(15, 'FirstLogin', '2020-04-12'),
(16, 'FirstLogin', '2020-04-12'),
(17, 'FirstLogin', '2020-04-12'),
(18, 'FirstLogin', '2020-04-12'),
(19, 'FirstLogin', '2020-04-12'),
(20, 'FirstLogin', '2020-04-12'),
(21, 'FirstLogin', '2020-04-12'),
(22, 'FirstLogin', '2020-04-12'),
(23, 'FirstLogin', '2020-04-13'),
(24, 'FirstLogin', '2020-04-13'),
(25, 'FirstLogin', '2020-04-13'),
(26, 'FirstLogin', '2020-04-13'),
(27, 'FirstLogin', '2020-04-13'),
(28, 'FirstLogin', '2020-04-13'),
(29, 'FirstLogin', '2020-04-13'),
(30, 'FirstLogin', '2020-04-13'),
(31, 'FirstLogin', '2020-04-13'),
(32, 'FirstLogin', '2020-04-13'),
(33, 'FirstLogin', '2020-04-13'),
(34, 'FirstLogin', '2020-04-13'),
(35, 'FirstLogin', '2020-04-13'),
(36, 'FirstLogin', '2020-04-13'),
(37, 'FirstLogin', '2020-04-13'),
(38, 'FirstLogin', '2020-04-13'),
(39, 'FirstLogin', '2020-04-13'),
(40, 'FirstLogin', '2020-04-13'),
(41, 'FirstLogin', '2020-04-13'),
(42, 'FirstLogin', '2020-04-13'),
(43, 'FirstLogin', '2020-04-13'),
(44, 'FirstLogin', '2020-04-13'),
(45, 'FirstLogin', '2020-04-13'),
(46, 'FirstLogin', '2020-04-13'),
(47, 'FirstLogin', '2020-04-13'),
(48, 'FirstLogin', '2020-04-13'),
(49, 'FirstLogin', '2020-04-13'),
(50, 'FirstLogin', '2020-04-13'),
(51, 'FirstLogin', '2020-04-13'),
(52, 'FirstLogin', '2020-04-13'),
(53, 'FirstLogin', '2020-04-13'),
(54, 'FirstLogin', '2020-04-13'),
(55, 'FirstLogin', '2020-04-13'),
(56, 'FirstLogin', '2020-04-13'),
(57, 'FirstLogin', '2020-04-13'),
(58, 'FirstLogin', '2020-04-13'),
(59, 'FirstLogin', '2020-04-13'),
(60, 'FirstLogin', '2020-04-13'),
(61, 'FirstLogin', '2020-04-13'),
(62, 'FirstLogin', '2020-04-13'),
(63, 'FirstLogin', '2020-04-13'),
(64, 'FirstLogin', '2020-04-13'),
(65, 'FirstLogin', '2020-04-13'),
(66, 'FirstLogin', '2020-04-13'),
(67, 'FirstLogin', '2020-04-13'),
(68, 'FirstLogin', '2020-04-13'),
(69, 'FirstLogin', '2020-04-13'),
(70, 'FirstLogin', '2020-04-13'),
(71, 'FirstLogin', '2020-04-13'),
(72, 'FirstLogin', '2020-04-14'),
(73, 'FirstLogin', '2020-04-14'),
(74, 'FirstLogin', '2020-04-14'),
(75, 'FirstLogin', '2020-04-14'),
(76, 'FirstLogin', '2020-04-14');

-- --------------------------------------------------------

--
-- Table structure for table `TourGroups`
--

CREATE TABLE `TourGroups` (
  `Id` int(11) NOT NULL,
  `TravelProductId` int(11) NOT NULL,
  `TourGuideId` int(11) NOT NULL DEFAULT '1',
  `DepDate` date NOT NULL,
  `CurrentOrderedPeople` int(11) NOT NULL DEFAULT '0',
  `Status` enum('New','Ready','Cancel','Setout') CHARACTER SET armscii8 NOT NULL DEFAULT 'New'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TourGroups`
--

INSERT INTO `TourGroups` (`Id`, `TravelProductId`, `TourGuideId`, `DepDate`, `CurrentOrderedPeople`, `Status`) VALUES
(2, 5, 1, '2020-04-25', 8, 'New'),
(5, 11, 1, '2020-05-09', 35, 'Ready'),
(6, 5, 3, '2020-03-02', 9, 'Setout'),
(14, 9, 1, '2020-04-22', 4, 'New'),
(17, 14, 5, '2020-04-16', 0, 'New'),
(18, 11, 1, '2020-04-17', 0, 'New');

-- --------------------------------------------------------

--
-- Table structure for table `TravelProducts`
--

CREATE TABLE `TravelProducts` (
  `Id` int(11) NOT NULL,
  `ProdName` varchar(100) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `MinPeople` int(11) NOT NULL,
  `Maxpeople` int(11) NOT NULL,
  `Description` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TravelProducts`
--

INSERT INTO `TravelProducts` (`Id`, `ProdName`, `Price`, `MinPeople`, `Maxpeople`, `Description`) VALUES
(5, 'Tornto 3 days travels', '250', 50, 60, '1. First day  2. Second day, 3 Third dayS'),
(9, 'Ottawa one day', '30', 33, 60, 'Ottawa one day tour'),
(11, 'Montreal 1 Day', '11', 30, 45, 'Old port Mont-royal'),
(14, 'Quebec city one day', '30', 35, 54, 'Text herejfejfiewjfwef');

-- --------------------------------------------------------

--
-- Stand-in structure for view `View_Customers`
-- (See below for the actual view)
--
CREATE TABLE `View_Customers` (
`Id` int(11)
,`Name` varchar(50)
,`Address` varchar(103)
,`PostCode` varchar(10)
,`Phone` varchar(24)
,`Email` varchar(50)
,`RegesterDate` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `View_Employees`
-- (See below for the actual view)
--
CREATE TABLE `View_Employees` (
`Id` int(11)
,`EName` varchar(50)
,`Address` varchar(108)
,`PostCode` varchar(10)
,`Phone` varchar(25)
,`Email` varchar(50)
,`HireDate` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `View_OrderDetails`
-- (See below for the actual view)
--
CREATE TABLE `View_OrderDetails` (
`Id` int(11)
,`Description` varchar(100)
,`NumberOfPeople` int(11)
,`OrderDate` date
,`Status` varchar(7)
,`CustomerName` varchar(50)
,`CustomerPhone` varchar(24)
,`ProductName` varchar(100)
,`Price` decimal(10,0)
,`DepDate` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_TourDetails`
-- (See below for the actual view)
--
CREATE TABLE `view_TourDetails` (
`TId` int(11)
,`ProductName` varchar(100)
,`MaxPeople` int(11)
,`MinPeople` int(11)
,`DepartDate` date
,`TourStatus` enum('New','Ready','Cancel','Setout')
,`CurrOrdered` int(11)
,`TourGuideName` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `View_Customers`
--
DROP TABLE IF EXISTS `View_Customers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `View_Customers`  AS  select `C`.`Id` AS `Id`,`C`.`Name` AS `Name`,concat_ws(',',`C`.`Address`,`C`.`City`,`C`.`State`,`C`.`Country`) AS `Address`,`C`.`PostCode` AS `PostCode`,`C`.`Phone` AS `Phone`,`C`.`Email` AS `Email`,`C`.`RegesterDate` AS `RegesterDate` from `Customers` `C` ;

-- --------------------------------------------------------

--
-- Structure for view `View_Employees`
--
DROP TABLE IF EXISTS `View_Employees`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `View_Employees`  AS  select `E`.`Id` AS `Id`,`E`.`EName` AS `EName`,concat_ws(',',`E`.`Address`,`E`.`City`,`E`.`State`,`E`.`Country`) AS `Address`,`E`.`PostCode` AS `PostCode`,`E`.`Phone` AS `Phone`,`E`.`Email` AS `Email`,`E`.`HireDate` AS `HireDate` from `Employees` `E` ;

-- --------------------------------------------------------

--
-- Structure for view `View_OrderDetails`
--
DROP TABLE IF EXISTS `View_OrderDetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `View_OrderDetails`  AS  select `O`.`Id` AS `Id`,`O`.`Description` AS `Description`,`O`.`NumberOfPeople` AS `NumberOfPeople`,`O`.`OrderDate` AS `OrderDate`,(case when (`O`.`Status` = 1) then 'Pending' when (`O`.`Status` = 2) then 'Done' when (`O`.`Status` = 3) then 'Cancel' end) AS `Status`,`C`.`Name` AS `CustomerName`,`C`.`Phone` AS `CustomerPhone`,`TP`.`ProdName` AS `ProductName`,`TP`.`Price` AS `Price`,`TG`.`DepDate` AS `DepDate` from (((`Orders` `O` join `Customers` `C` on((`C`.`Id` = `O`.`CustomerId`))) join `TourGroups` `TG` on((`TG`.`Id` = `O`.`TourGroupId`))) join `TravelProducts` `TP` on((`TG`.`TravelProductId` = `TP`.`Id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_TourDetails`
--
DROP TABLE IF EXISTS `view_TourDetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_TourDetails`  AS  select `TG`.`Id` AS `TId`,`TP`.`ProdName` AS `ProductName`,`TP`.`Maxpeople` AS `MaxPeople`,`TP`.`MinPeople` AS `MinPeople`,`TG`.`DepDate` AS `DepartDate`,`TG`.`Status` AS `TourStatus`,`TG`.`CurrentOrderedPeople` AS `CurrOrdered`,`E`.`EName` AS `TourGuideName` from ((`TourGroups` `TG` join `TravelProducts` `TP` on((`TG`.`TravelProductId` = `TP`.`Id`))) join `Employees` `E` on((`TG`.`TourGuideId` = `E`.`Id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customers`
--
ALTER TABLE `Customers`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Employees`
--
ALTER TABLE `Employees`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `TourGroupId` (`TourGroupId`),
  ADD KEY `CustomerId` (`CustomerId`);

--
-- Indexes for table `TaskLogs`
--
ALTER TABLE `TaskLogs`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `TourGroups`
--
ALTER TABLE `TourGroups`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_tour_Employee` (`TourGuideId`),
  ADD KEY `fk_tour_TravelProd` (`TravelProductId`);

--
-- Indexes for table `TravelProducts`
--
ALTER TABLE `TravelProducts`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customers`
--
ALTER TABLE `Customers`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `Employees`
--
ALTER TABLE `Employees`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `TaskLogs`
--
ALTER TABLE `TaskLogs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `TourGroups`
--
ALTER TABLE `TourGroups`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `TravelProducts`
--
ALTER TABLE `TravelProducts`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
  ADD CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`TourGroupId`) REFERENCES `TourGroups` (`Id`),
  ADD CONSTRAINT `Orders_ibfk_2` FOREIGN KEY (`CustomerId`) REFERENCES `Customers` (`Id`);

--
-- Constraints for table `TourGroups`
--
ALTER TABLE `TourGroups`
  ADD CONSTRAINT `fk_tour_Employee` FOREIGN KEY (`TourGuideId`) REFERENCES `Employees` (`Id`),
  ADD CONSTRAINT `fk_tour_TravelProd` FOREIGN KEY (`TravelProductId`) REFERENCES `TravelProducts` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
