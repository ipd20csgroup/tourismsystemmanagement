﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for DialogCustomerAddUpdate.xaml
    /// </summary>
    public partial class DialogCustomerAddUpdate : Window
    {
        Customer currentCustomer;
        public DialogCustomerAddUpdate(View_Customers viewCustomer)
        {
            InitializeComponent();
            if (viewCustomer != null)
            {
                currentCustomer = (from e in Globals.Db.Customers where e.Id == viewCustomer.Id select e).FirstOrDefault<Customer>();
                btDlgCustomerAddEdit.Content = "Update";
                lblDlgCustomerId.Content = currentCustomer.Id + "";
                tbDlgCustomerName.Text = currentCustomer.Name;
                tbDlgCustomerAddress.Text = currentCustomer.Address;
                tbDlgCustomerCity.Text = currentCustomer.City;
                tbDlgCustomerState.Text = currentCustomer.State;
                tbDlgCustomerCountry.Text = currentCustomer.Country;
                tbDlgCustomerPostCode.Text = currentCustomer.PostCode;
                tbDlgCustomerPhone.Text = currentCustomer.Phone;
                tbDlgCustomerEmail.Text = currentCustomer.Email;
                dpDlgCustomerRegDate.SelectedDate = currentCustomer.RegesterDate;
            }
            else
            {
                btDlgCustomerAddEdit.Content = "Create";
            }
        }

        private void BtDlgCustomerAddEdit_Click(object sender, RoutedEventArgs e)
        {
            string name = tbDlgCustomerName.Text;
            if (name.Length < 1 || name.Length > 50)
            {
                MessageBox.Show(this, "Name must be 1-50.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string address = tbDlgCustomerAddress.Text;
            if (address.Length < 1 || address.Length > 60)
            {
                MessageBox.Show(this, "Address must be 1-60.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string city = tbDlgCustomerCity.Text;
            if (city.Length < 1 || city.Length > 15)
            {
                MessageBox.Show(this, "City must be 1-15.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string state = tbDlgCustomerState.Text;
            if (state.Length < 1 || state.Length > 10)
            {
                MessageBox.Show(this, "Customer state must be 1-10.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string country = tbDlgCustomerCountry.Text;
            if (country.Length < 1 || country.Length > 15)
            {
                MessageBox.Show(this, "Country must be 1-15.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string postCode = tbDlgCustomerPostCode.Text;
            if (postCode.Length < 1 || postCode.Length > 10)
            {
                MessageBox.Show(this, "PostCode must be 1-10.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string phone = tbDlgCustomerPhone.Text;
            if (phone.Length < 1 || phone.Length > 24)
            {
                MessageBox.Show(this, "Phone must be 1-15.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string email = tbDlgCustomerEmail.Text;
            if (email.Length < 1 || email.Length > 50)
            {
                MessageBox.Show(this, "Email must be 1-50.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DateTime regesterDate = dpDlgCustomerRegDate.SelectedDate.Value;

            if (currentCustomer != null)
            {
                try
                {
                    Customer customer = Globals.Db.Customers.Where(c => c.Id == currentCustomer.Id).FirstOrDefault();
                    customer.Name = name;
                    customer.Address = address;
                    customer.City = city;
                    customer.State = state;
                    customer.Country = country;
                    customer.PostCode = postCode;
                    customer.Phone = phone;
                    customer.Email = email;
                    customer.RegesterDate = regesterDate;
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Error saving record:\n" + ex.Message);
                }
            }
            else
            {
                try
                {
                    Globals.Db.Customers.Add(new Customer() { Name = name, Address = address, City = city, State = state, Country = country,PostCode=postCode, Phone = phone, Email = email, RegesterDate = regesterDate });
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Error saving record:\n" + ex.Message);
                }
            }
          this.DialogResult = true; // hide dialog
        }

        private void BtDlgCustomerDelete_Click(object sender, RoutedEventArgs e)
        {
           // MainPage.currentIndex = 1;
            Close();
        }
    }
}
