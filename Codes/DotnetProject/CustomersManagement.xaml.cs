﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for CustomersManagement.xaml
    /// </summary>
    public partial class CustomersManagement : Page
    {
        static List<View_Customers> customersList = new List<View_Customers>();
        public CustomersManagement()
        {
            InitializeComponent();
            LoadDataFromDB(null);
            btUpdateCustomer.IsEnabled = false;
            btDeleteCustomer.IsEnabled = false;
        }

        string filter;
        void LoadDataFromDB(string filter)
        {
            try
            {
                customersList = (from c in Globals.Db.View_Customers select c).ToList();
                if (filter != null)
                { 
                    customersList = customersList.Where(s => s.Name.Contains(filter) || 
                    s.Address.Contains(filter) || s.PostCode.Contains(filter) || s.Email.Contains(filter)).ToList();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error records:\n" + ex.Message);
            }
            lvCustomers.ItemsSource = customersList;
        }

        private void BtAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            DialogCustomerAddUpdate dlgCustomerAddUpdate = new DialogCustomerAddUpdate(null);
            dlgCustomerAddUpdate.ShowDialog();
            LoadDataFromDB(filter);
        }

        private void LvCustomers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            View_Customers customer = (View_Customers)lvCustomers.SelectedValue;
            if (customer == null)
            {
                return;
            }
            DialogCustomerAddUpdate dlgCustomerAddUpdate = new DialogCustomerAddUpdate(customer);
            dlgCustomerAddUpdate.ShowDialog();
            LoadDataFromDB(filter);
        }

        private void LvCustomers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btUpdateCustomer.IsEnabled = true;
            btDeleteCustomer.IsEnabled = true;
        }

        private void BtUpdateCustomer_Click(object sender, RoutedEventArgs e)
        {
            View_Customers customer = (View_Customers)lvCustomers.SelectedValue;
            if (customer == null)
            {
                return;
            }
            DialogCustomerAddUpdate dlgCustomerAddUpdate = new DialogCustomerAddUpdate(customer);
            dlgCustomerAddUpdate.ShowDialog();
            LoadDataFromDB(filter);
        }
        View_Customers selectedCustomer;

        private void BtDeleteCustomer_Click(object sender, RoutedEventArgs e)
        {
            selectedCustomer = (View_Customers)lvCustomers.SelectedValue;
            if (selectedCustomer == null) {
                return;
            }
            Customer customerDel = (from c in Globals.Db.Customers where c.Id == selectedCustomer.Id select c).FirstOrDefault<Customer>();

            //check if customer has order, cann't be delete
            Order orderByCustomer = (from o in Globals.Db.Orders where o.CustomerId == selectedCustomer.Id select o).FirstOrDefault<Order>();
            if (orderByCustomer != null)
            {
                MessageBox.Show("Customer " + customerDel.Name + " has orders,please delete orders first!", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (customerDel != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this item?\n" + customerDel.Name, "Delete confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.OK)
                {
                    Globals.Db.Customers.Remove(customerDel); // schedule for deletion from database
                    Globals.Db.SaveChanges();
                    LoadDataFromDB(filter);
                }
            }
            else
            {
                Console.WriteLine("record to delete not found");
            }
        }


        private void TbSearch_KeyDown(object sender, KeyEventArgs e)
        {
            filter = tbSearch.Text;
            LoadDataFromDB(filter);
        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            filter = tbSearch.Text;
            LoadDataFromDB(filter);
        }

        private void BtExportCustomers_Click(object sender, RoutedEventArgs e)
        {
            PublicMethod.ExportToCsv<View_Customers>(customersList);
        }
    }
}
