﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for DialogTourGroupAddUpdate.xaml
    /// </summary>
    public partial class DialogTourGroupAddUpdate : Window
    {
        TourGroup currTour;
        view_TourDetails selected;
        Boolean isDone = false;
        List<TravelProduct> tpList;
   
        public enum TourStatsEnum { New, Ready, Cancel, Setout }//FixMe
        public DialogTourGroupAddUpdate(view_TourDetails s)
        {
            InitializeComponent();
            getAllDataFromDBToListView();
            tgd_cbTourStatus.ItemsSource = Enum.GetValues(typeof(TourStatsEnum)); //Fixme: generate Enum class from DB and banding to combobox
            tgd_cbTourGuideName.ItemsSource = (from e in Globals.Db.Employees select e.EName).ToList();
            selected = s;
            if (selected != null)
            {
                currTour = (TourGroup)(from t in Globals.Db.TourGroups from v in Globals.Db.view_TourDetails where t.Id == selected.TId select t).ToList().First();
                
                tgd_lblTitle.Content = "Update Tour Group";
                //TPD_lblProdId.Content = currTour.ProductName;
                tgd_lblTourId.Content = currTour.Id;
                tgd_lblProdId.Content = currTour.TravelProductId;
                tgd_lblCurrentOrderedNum.Content = selected.CurrOrdered.ToString();
                tgd_tbTourGuideId.Text = currTour.TourGuideId.ToString();
                tgd_dpDepartDate.SelectedDate = currTour.DepDate;
                tgd_cbTourGuideName.Text = selected.TourGuideName;
                tgd_cbTourStatus.Text = currTour.Status; //to Fix
                tgd_lblTravelProdName.Content = selected.ProductName;
                lvCurrentProds.SelectedItem = (TravelProduct)(from p in Globals.Db.TravelProducts where p.Id == currTour.TravelProductId select p).First();
                tgd_btCreateUpdate.Content = "Update";
            }
            else
            {
                tgd_lblTitle.Content = "Create Tour Group";
                tgd_dpDepartDate.SelectedDate = DateTime.Now;
                tgd_btCreateUpdate.Content = "Create";
                tgd_cbTourStatus.SelectedIndex = 0;
                tgd_cbTourGuideName.SelectedIndex = 0;
                tgd_tbTourGuideId.Text = "1";
            }
        }

        private void getAllDataFromDBToListView()
        {
            lvCurrentProds.ItemsSource = null;
            lvCurrentProds.Items.Clear();
            try
            {
                tpList = (from tp in Globals.Db.TravelProducts select tp).ToList();
                lvCurrentProds.ItemsSource = tpList;
                this.lvCurrentProds.SelectedItems.Clear();
                TD_tbProdSearch.Text = "";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Load Data from Database Error:\n" + ex.Message);
            }
        }

        private void tgd_btCreateUpdate_Click(object sender, RoutedEventArgs e)
        {
            int tpId;
            int.TryParse(tgd_lblProdId.Content.ToString(), out tpId);
            int tGuideId;
            int.TryParse(tgd_tbTourGuideId.Text, out tGuideId);
            int pId;
            int.TryParse(tgd_lblProdId.Content.ToString(), out pId);
            DateTime dpDate;
            DateTime.TryParse(tgd_dpDepartDate.Text, out dpDate);
            string tourstatus = tgd_cbTourStatus.Text;

            if (tpId < 1)
            {
                MessageBox.Show(this, "Must choose one Travel product\n", "Argument Error:", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (tGuideId < 1)
            {
                MessageBox.Show(this, "Invalid Tour Guide ID\n", "Input Error:", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpDate.CompareTo(DateTime.Now) < 0)
            {
                MessageBox.Show(this, "Date selected must be greater than current date\n", "Date selected error:", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (selected == null)//add new 
            {
                try
                {
                    TourGroup t = new TourGroup { TravelProductId = tpId, TourGuideId = tGuideId, DepDate = dpDate, Status = tourstatus };
                    Globals.Db.TourGroups.Add(t);
                    isDone = true;
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, "Create error: \n" + ex.Message, "Create error", MessageBoxButton.OK, MessageBoxImage.Error);
                    isDone = false;
                    return;
                }
                Globals.Db.SaveChanges();
            }
            else //update
            {
                try
                {
                    int tid;
                    int.TryParse(tgd_lblTourId.Content.ToString(), out tid);
                    TourGroup selectedTg = Globals.Db.TourGroups.Where(t => t.Id == tid).FirstOrDefault();//FirstOrDefault(tpd => tpd.Id == currentTp.Id);  //FirstOrDefault);   //EFTableSet user = ef.EFTableSet.Where(m => m.Id == id).FirstOrDefault();//FirstOrDefault查询第一个
                    selectedTg.Status = tourstatus;
                    selectedTg.TravelProductId = tpId;
                    selectedTg.TourGuideId = tGuideId;
                    selectedTg.DepDate = dpDate;
                    isDone = true;
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, "Update error:\n" + ex.Message, "Update error", MessageBoxButton.OK, MessageBoxImage.Error);
                    isDone = false;
                    return;
                }
                Globals.Db.SaveChanges();
            }
            if (isDone) DialogResult = true;

        }


        private void tgd_tbTourGuideId_KeyDown(object sender, KeyEventArgs e)
        {
            int tourGuideId;
            if (int.TryParse(tgd_tbTourGuideId.Text, out tourGuideId))
            {
                try
                {
                    Employee tourGuide = (Employee)(from en in Globals.Db.Employees where en.Id == tourGuideId select en).First();
                    tgd_cbTourGuideName.Text = tourGuide.EName;
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Invalid Input: Employee Id not exist:\n" + ex.Message);
                }

            }
            else
            {
                tgd_cbTourGuideName.SelectedIndex = 0;
            }
        }

        private void lvCurrentProds_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TravelProduct tp = (TravelProduct)lvCurrentProds.SelectedItem;
            if (tp != null)
            {
                tgd_lblProdId.Content = tp.Id;
                tgd_lblTravelProdName.Content = tp.ProdName;
            }
            else
            {
                tgd_lblProdId.Content = "n/a";
                tgd_lblTravelProdName.Content = "n/a";
            }
        }

        private void TD_tbProdSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string keyWords = TD_tbProdSearch.Text.Trim().ToLower();
            if (lvCurrentProds == null) return;
            if (keyWords != "")
            {
                lvCurrentProds.ItemsSource = null;
                lvCurrentProds.Items.Clear();
                foreach (TravelProduct tp in tpList)
                {
                    decimal p = tp.Price;
                    string pstr = p.ToString();
                    if (tp.ProdName.ToLower().Contains(keyWords) || tp.Price.ToString().Contains(keyWords) || tp.Maxpeople.ToString().Contains(keyWords) || tp.MinPeople.ToString().Contains(keyWords))
                    {
                        lvCurrentProds.Items.Add(tp);
                    }
                }
            }
            else
            {
                getAllDataFromDBToListView();
            }
        }

        private void tgd_cbTourGuideName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String tourGuideName = tgd_cbTourGuideName.SelectedItem.ToString();
            if (tourGuideName != null || tourGuideName != "")
            {
                try
                {
                    Employee tourGuide = (Employee)(from en in Globals.Db.Employees where en.EName == tourGuideName select en).First();
                    tgd_tbTourGuideId.Text = tourGuide.Id.ToString();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Selceted Erro: please check:\n" + ex.Message);
                }
            }
            else return;
        }
    }
}
