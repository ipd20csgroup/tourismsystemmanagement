﻿namespace DotnetProject
{
    internal class TourGroupSelected
    {
        public TourGroupSelected()
        {
        }

        public object TId { get; set; }
        public object Date { get; set; }
        public object CurrOrdered { get; set; }
        public object Sts { get; set; }
        public object Name { get; set; }
        public object TourGuideId { get; set; }
    }
}