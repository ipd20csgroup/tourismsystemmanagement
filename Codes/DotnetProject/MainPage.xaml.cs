﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        static List<View_Customers> customersList = new List<View_Customers>();
        static List<View_Employees> employeesList = new List<View_Employees>();
        static List<View_OrderDetails> ordersList = new List<View_OrderDetails>();

        public MainPage()
        {
            InitializeComponent();
            if (MainWindow.currentRole != "Admin") {
                miEmployees.Visibility = Visibility.Hidden;
            }
            lblUserName.Content = MainWindow.currentUserName;
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            int index = int.Parse(((Button)e.Source).Uid);
            GridCursor.Margin = new Thickness(10 + (150 * index), 0, 0, 0);
            switch (index)
            {
                case 0:
                    showFrame.Source = new Uri("CustomersManagement.xaml", UriKind.Relative);
                    break;
                case 1:
                    showFrame.Source = new Uri("OrdersManagement.xaml", UriKind.Relative);
                    break;
                case 2:
                    showFrame.Source = new Uri("ToursManagement.xaml", UriKind.Relative);
                    break;
                case 3:
                    showFrame.Source = new Uri("TravelProductManagement.xaml", UriKind.Relative);
                    break;
                case 4:
                    showFrame.Source = new Uri("EmployeesManagement.xaml", UriKind.Relative);
                    break;
                default:
                    showFrame.Source = new Uri("CustomersManagement.xaml", UriKind.Relative);
                    break;
            }
        }
        
        private void MiExit_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}
