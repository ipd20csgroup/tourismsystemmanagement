﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for ToursManagement.xaml
    /// </summary>
    public partial class ToursManagement : Page
    {

        view_TourDetails selected = null;
        List<view_TourDetails> tglist;
        List<TourGroupSelected> itemsList = new List<TourGroupSelected>();

        public ToursManagement()
        {
            InitializeComponent();
            getAllTourGroupsDataFromDBToListView();
        }

        private void getAllTourGroupsDataFromDBToListView()
        {
            lvTours.ItemsSource = null;
            lvTours.Items.Clear();
            try
            {
                tglist = (from t in Globals.Db.view_TourDetails select t).ToList();
                lvTours.ItemsSource = tglist;
                this.lvTours.SelectedItems.Clear();
                btDeleteTour.IsEnabled = false;
                btUpdateTour.IsEnabled = false;
                tbSearchTour.Text = "";
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show("Load Data from Database Error:\n" + ex.Message);
            }
        }

        private void saveDataToFile()
        {
            if (lvTours.SelectedIndex != -1)
            {
                string[] dataLine = new string[lvTours.SelectedItems.Count];
                int index = 0;
                foreach (view_TourDetails vt in lvTours.SelectedItems)
                {
                    dataLine[index++] = "TourId:" + vt.TId + "; Product Name:" + vt.ProductName + "; Departure Date:" + vt.DepartDate + ";Tour Guide Name:" + vt.TourGuideName + ";Current ordered:" + vt.CurrOrdered + ";Tour Status:" + vt.TourStatus;
                }
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text files (*.csv)|*.csv|All files (*.*)|*.*";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        File.WriteAllLines(saveFileDialog.FileName, dataLine);
                    }
                    catch (IOException ex)
                    {
                        Globals.exlog.Write(ex);
                        MessageBox.Show("Error writing file:\n" , "File access error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select data first", "Data select", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        private void lvTours_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvTours.SelectedIndex != -1)
            {
                selected = (view_TourDetails)lvTours.SelectedItem;
                //DialogGroupTourAddUpdate dlgTPUpdateAdd = new DialogGroupTourAddUpdate(selected);
                DialogTourGroupAddUpdate dlgGtUpdateAdd = new DialogTourGroupAddUpdate(selected);
                if (dlgGtUpdateAdd.ShowDialog() == true)
                {
                    getAllTourGroupsDataFromDBToListView();
                }
                else
                {
                    this.lvTours.SelectedItems.Clear();
                }
            }
            else
            {
                selected = null;
                return;
            }
        }

        private void lvTours_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvTours.SelectedItems != null)
            {
                btDeleteTour.IsEnabled = true;
                btUpdateTour.IsEnabled = true;
            }
            else
            {
                btDeleteTour.IsEnabled = false;
                btUpdateTour.IsEnabled = false;
            }
        }

        private void btAddTour_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(lvTourGroups.SelectedItem.GetType().ToString());
            DialogTourGroupAddUpdate dlgTPUpdateAdd = new DialogTourGroupAddUpdate(null);

            dlgTPUpdateAdd.ShowDialog();
            getAllTourGroupsDataFromDBToListView(); //Tofix: refresh after updata data
        }

        private void btUpdateTour_Click(object sender, RoutedEventArgs e)
        {
            if (lvTours.SelectedIndex != -1)
            {
                selected = (view_TourDetails)lvTours.SelectedItem;
            }
            else
            {
                selected = null;
            }
            DialogTourGroupAddUpdate dlgTPUpdateAdd = new DialogTourGroupAddUpdate(selected);

            dlgTPUpdateAdd.ShowDialog();
            getAllTourGroupsDataFromDBToListView(); //Tofix: refresh after updata data
        }

        private void btDeleteTour_Click(object sender, RoutedEventArgs e)
        {
            delete();
        }

        private void miSaveSelectedData_Context(object sender, RoutedEventArgs e)
        {
            saveDataToFile();
        }

        private void miDelete_Context(object sender, RoutedEventArgs e)
        {
            delete();
        }

        private void delete() {
            if (lvTours.SelectedIndex != -1)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete this selected data \n", "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    try
                    {
                        selected = (view_TourDetails)lvTours.SelectedItem;
                        var currentTour = (from t in Globals.Db.TourGroups.Include("Orders") where t.Id == selected.TId select t).First();
                        //Globals.Db.Entry(currentTour).Collection(d => d.Orders).Load();
                        currentTour.TravelProductId = 0;
                        currentTour.TourGuideId = 0;
                        Globals.Db.TourGroups.Remove(currentTour);
                        Globals.Db.SaveChanges();
                        MessageBox.Show("Delete Successed.", "Delete Successed", MessageBoxButton.OK, MessageBoxImage.Information);

                        getAllTourGroupsDataFromDBToListView();
                    }
                    catch (SystemException ex)
                    {
                        Globals.exlog.Write(ex);
                        MessageBox.Show("This tour has orders ,can not be deleted\n", "Deletion failed", MessageBoxButton.OK, MessageBoxImage.Error);
                        this.lvTours.SelectedItems.Clear();
                        btDeleteTour.IsEnabled = false;
                        btUpdateTour.IsEnabled = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select one tour to Delete.", "Selected error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void tbSearchTour_TextChanged(object sender, TextChangedEventArgs e)
        {
                string keyWords = tbSearchTour.Text.Trim().ToLower();
                if (lvTours == null) return;
                if (keyWords != "")
                {
                    lvTours.ItemsSource = null;
                    lvTours.Items.Clear();
                    foreach (view_TourDetails tg in tglist)//to fix date match
                    {
                        if (tg.ProductName.ToLower().Contains(keyWords) || tg.DepartDate.ToString().Contains(keyWords) || tg.CurrOrdered.ToString().Equals(keyWords) || tg.TourStatus.ToLower().Contains(keyWords) || tg.TourGuideName.ToLower().Contains(keyWords))
                        {
                            lvTours.Items.Add(tg);
                        }
                    }
                }
                else
                {
                    getAllTourGroupsDataFromDBToListView();
                }
        }

        private void BtExportGroup_Click(object sender, RoutedEventArgs e)
        {
            PublicMethod.ExportToCsv<view_TourDetails>(tglist);
        }
    }
}
