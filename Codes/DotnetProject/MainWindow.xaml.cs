﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public static string currentRole;
        public static string currentUserName;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = btUserName.Text;
            string passwordOri = pwPassWord.Password;
            string passwordMd5 = PublicMethod.MD5Encrypt32(passwordOri);  //encryption password

            Employee currentUser = (from c in Globals.Db.Employees where (c.Username == username) && (c.Password == passwordMd5) select c).FirstOrDefault<Employee>();

            if (currentUser != null)
            {
                currentRole = currentUser.RoleLevel;
                currentUserName = currentUser.Username;

                //check if it is first time login ,if yes ,update group and order status, and add First login log
                string taskName = "FirstLogin";
                DateTime date = DateTime.Now;
                try
                {
                    TaskLog task = (from t in Globals.Db.TaskLogs where (t.Task == taskName) && (t.Date == date) select t).FirstOrDefault<TaskLog>();
                    if (task == null)
                    {
                        List<TourGroup> groupsList = (from g in Globals.Db.TourGroups where (g.DepDate < date) && (g.Status == "Ready") select g).ToList();
                        if (groupsList != null)
                        {
                            foreach (TourGroup tg in groupsList)
                            {
                                // set group status is set out if depate date is before today
                                TourGroup group = Globals.Db.TourGroups.Where(gp => gp.Id == tg.Id).FirstOrDefault();
                                group.Status = "Setout";
                                Globals.Db.SaveChanges();

                                //set order status is done for every group depart date if before today
                                Order order = Globals.Db.Orders.Where(o => o.TourGroupId == tg.Id).FirstOrDefault();
                                if (order != null)
                                {
                                    order.Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), "Done");
                                    Globals.Db.SaveChanges();
                                }
                            }
                        }
                        //add TaskLogs, for only the first login 
                        Globals.Db.TaskLogs.Add(new TaskLog() { Task = taskName, Date = date });
                        Globals.Db.SaveChanges();
                    }
                    MainPage mainPage = new MainPage();
                    mainPage.Show();
                    this.Close();
                }
                catch (SystemException ex) {
                    MessageBox.Show("login database error:\n" + ex.Message); 
                }

            }
            else {
                MessageBox.Show(this, "Wrong username or password", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

        }

        private void BtLoginCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
