﻿using System;

namespace DotnetProject
{
    internal class TourSelectedForOrder
    {
        public TourSelectedForOrder()
        {
        }

        public int TId { get; set; }
        public DateTime Date { get; set; }
        public int CurrOrdered { get; set; }
        public string Sts { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}