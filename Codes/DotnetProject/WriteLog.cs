﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotnetProject
{
    public class WriteLog
    {
        /// <summary>
        /// Print exception to file
        /// </summary>
        /// <param name="ex">Exception</param>
        /// <param name="LogAddress">Log file address</param>
        public void Write(Exception ex, string LogAddress = "")
        {
            //If log file is not exsit，then default to create log under the Debug dir as format " YYYY-mm-dd_Log.log "
            if (LogAddress == "")
            {
                LogAddress = Environment.CurrentDirectory + '\\' +
                    DateTime.Now.Year + '-' +
                    DateTime.Now.Month + '-' +
                    DateTime.Now.Day + "_Log.log";
            }
            //Write exception into the log file，
            StreamWriter fs = new StreamWriter(LogAddress, true);
            fs.WriteLine("Current Time：" + DateTime.Now.ToString());
            fs.WriteLine("Exception Info：" + ex.Message);
            fs.WriteLine("Exception Object：" + ex.Source);
            fs.WriteLine("Invock Stack：\n" + ex.StackTrace.Trim());
            fs.WriteLine("Triggering Method：" + ex.TargetSite);
            fs.WriteLine();
            fs.Close();
        }


    }
}
