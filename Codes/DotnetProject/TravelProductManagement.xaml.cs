﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for TravelProductManagement.xaml
    /// </summary>
    public partial class TravelProductManagement : Page
    {
        TravelProduct selected = null;
        List<TravelProduct> tplist;

        public TravelProductManagement()
        {
            InitializeComponent();
            getAllDataFromDBToListView();
        }


        private void getAllDataFromDBToListView()
        {
            lvTravelProducts.ItemsSource = null;
            lvTravelProducts.Items.Clear();
            try
            {
                tplist = (from tp in Globals.Db.TravelProducts select tp).ToList();
                lvTravelProducts.ItemsSource = tplist;

                this.lvTravelProducts.SelectedItems.Clear();
                btDeleteTravelProd.IsEnabled = false;
                btUpdateTravelProd.IsEnabled = false;
                tbSearchTravelProd.Text = "";
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Load Data from Database Error:\n" + ex.Message);
            }
        }

        private void saveDataToFile()
        {
            if (lvTravelProducts.SelectedIndex != -1)
            {
                string[] dataLine = new string[lvTravelProducts.SelectedItems.Count];
                int index = 0;
                foreach (TravelProduct tp in lvTravelProducts.SelectedItems)
                {
                    dataLine[index++] = "Id:" + tp.Id + "; Prodct Name:" + tp.ProdName + "; Price:" + tp.Price + "; MinPeople:" + tp.MinPeople + "; MaxPeople:" + tp.Maxpeople;
                }
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text files (*.csv)|*.csv|All files (*.*)|*.*";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        File.WriteAllLines(saveFileDialog.FileName, dataLine);
                    }
                    catch (IOException ex)
                    {
                        Globals.exlog.Write(ex);
                        MessageBox.Show("Error writing file:\n", "File access error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select data first", "Data select", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        private void btAddTravelProd_Click(object sender, RoutedEventArgs e)
        {
            DialogTravelProductAddUpdate dlgTPUpdateAdd = new DialogTravelProductAddUpdate(null);
            dlgTPUpdateAdd.ShowDialog();
            getAllDataFromDBToListView(); //Tofix: refresh after updata data

        }

        private void btUpdateTravelProd_Click(object sender, RoutedEventArgs e)
        {
            if (lvTravelProducts.SelectedIndex != -1)
            {
                selected = (TravelProduct)lvTravelProducts.SelectedItem;
                DialogTravelProductAddUpdate dlgTPUpdateAdd = new DialogTravelProductAddUpdate(selected);
                dlgTPUpdateAdd.ShowDialog();
                getAllDataFromDBToListView();
            }
            else
            {
                selected = null;
            }
            
        }

        private void btDeleteTravelProd_Click(object sender, RoutedEventArgs e)
        {
            delete();
        }

        private void lvTravelProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvTravelProducts.SelectedItems != null)
            {
                btDeleteTravelProd.IsEnabled = true;
                btUpdateTravelProd.IsEnabled = true;
            }
            else
            {
                btDeleteTravelProd.IsEnabled = false;
                btUpdateTravelProd.IsEnabled = false;

            }
        }

        private void lvTravelProducts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvTravelProducts.SelectedIndex != -1)
            {
                selected = (TravelProduct)lvTravelProducts.SelectedItem;
                DialogTravelProductAddUpdate dlgTPUpdateAdd = new DialogTravelProductAddUpdate(selected);
                if (dlgTPUpdateAdd.ShowDialog() == true)
                {
                    getAllDataFromDBToListView();

                }
                else
                {
                    this.lvTravelProducts.SelectedItems.Clear();
                }
            }
            else
            {
                selected = null;
                return;
            }

        }

        private void tbSearchTravelProd_TextChanged(object sender, TextChangedEventArgs e)
        {
            string keyWords = tbSearchTravelProd.Text.Trim().ToLower();
            if (lvTravelProducts == null) return;
            if (keyWords != "")
            {
                lvTravelProducts.ItemsSource = null;
                lvTravelProducts.Items.Clear();
                foreach (TravelProduct tp in tplist)
                {
                    decimal p = tp.Price;
                    string pstr = p.ToString();
                    if (tp.ProdName.ToLower().Contains(keyWords) || tp.Price.ToString().Contains(keyWords) || tp.Maxpeople.ToString().Contains(keyWords) || tp.MinPeople.ToString().Contains(keyWords))
                    {
                        lvTravelProducts.Items.Add(tp);
                    }
                }
            }
            else
            {
                getAllDataFromDBToListView();
            }
        }

        private void delete() 
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete this selected data \n", "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (lvTravelProducts.SelectedIndex != -1)
            {
                try
                {
                    selected = (TravelProduct)lvTravelProducts.SelectedItem;
                    Globals.Db.TravelProducts.Attach(selected);
                    Globals.Db.TravelProducts.Remove(selected);
                    Globals.Db.SaveChanges();
                    MessageBox.Show("Delete Successed.", "Delete Successed", MessageBoxButton.OK, MessageBoxImage.Information);
                    getAllDataFromDBToListView();
                }
                catch (SystemException ex)
                {
                    Globals.exlog.Write(ex);//Fix me
                    MessageBox.Show("Can not be delete,due to this Travel Product has tours", "Delete Failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                MessageBox.Show("Please select one product to Delete.", "Selected error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void miSaveSelectedData_Context(object sender, RoutedEventArgs e)
        {
            saveDataToFile();
        }

        private void miDelete_Context(object sender, RoutedEventArgs e)
        {
            delete();
        }
    }
}
