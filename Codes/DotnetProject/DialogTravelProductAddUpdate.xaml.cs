﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for DialogTravelProductAddUpdate.xaml
    /// </summary>
    public partial class DialogTravelProductAddUpdate : Window
    {
        TravelProduct currentTp;
        public DialogTravelProductAddUpdate(TravelProduct t)
        {

            InitializeComponent();
            if (t != null)
            {
                currentTp = t;
                TPD_lblTitelAddUpdateProduct.Content = "Update Travel Product";
                TPD_lblProdId.Content = currentTp.Id;
                TPD_tbProdName.Text = currentTp.ProdName;
                TPD_tbPrice.Text = currentTp.Price.ToString();
                TPD_tbMinPeople.Text = currentTp.MinPeople.ToString();
                TPD_tbMaxPeople.Text = currentTp.Maxpeople.ToString();
                TPD_tbDescription.Text = currentTp.Description.ToString();
                TPD_btCreateUpdate.Content = "Update";
            }
            else
            {
                TPD_lblTitelAddUpdateProduct.Content = "Create Travel Product";
                TPD_btCreateUpdate.Content = "Create";
            }
        }

        private void TPD_btCreateUpdate_Click(object sender, RoutedEventArgs e)
        {

            string prodName = TPD_tbProdName.Text;
            decimal price;
            decimal.TryParse(TPD_tbPrice.Text, out price);
            int minPeople;
            int.TryParse(TPD_tbMinPeople.Text, out minPeople);
            int maxPeople;
            int.TryParse(TPD_tbMaxPeople.Text, out maxPeople);
            string descrip = TPD_tbDescription.Text;
            if (prodName.Length < 0 || prodName.Length > 100)
            {//toFix:check size inDB
                MessageBox.Show(this, "Product Name must be 1-100.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (price <= 0)
            {
                MessageBox.Show(this, "Price must be greater than 0", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (minPeople <= 0 || maxPeople <= 0 || minPeople > maxPeople)
            {
                MessageBox.Show(this, "People number must be greater than 0 and Min greater than Max", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (descrip.Length == 0 || descrip.Equals("Text here"))
            {//toFix:check size in db
                MessageBox.Show(this, "Description required.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (currentTp != null)
            {
                try
                {
                    TravelProduct selectedTP = Globals.Db.TravelProducts.Where(p => p.Id == currentTp.Id).FirstOrDefault();//FirstOrDefault(tpd => tpd.Id == currentTp.Id);  //FirstOrDefault);   //EFTableSet user = ef.EFTableSet.Where(m => m.Id == id).FirstOrDefault();//FirstOrDefault查询第一个
                    selectedTP.ProdName = prodName;
                    selectedTP.Price = price;
                    selectedTP.MinPeople = minPeople;
                    selectedTP.Maxpeople = maxPeople;
                    selectedTP.Description = descrip;
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    Globals.exlog.Write(ex);
                    MessageBox.Show("Update faild."+ex.Message,"Update error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                try
                {
                    Globals.Db.TravelProducts.Add(new TravelProduct { ProdName = prodName, Price = price, MinPeople = minPeople, Maxpeople = maxPeople, Description = descrip });
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(null, "Create success.", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            DialogResult = true;
        }
    }
}
