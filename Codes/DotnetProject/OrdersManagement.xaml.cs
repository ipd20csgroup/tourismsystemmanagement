﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsvHelper;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for OrdersManagement.xaml
    /// </summary>
    public partial class OrdersManagement : Page
    {
        static List<View_OrderDetails> ordersList = new List<View_OrderDetails>();
        public OrdersManagement()
        {
            InitializeComponent();
            LoadDataFromDB(filter);          
            btUpdateOrder.IsEnabled = false;
            btDeleteOrder.IsEnabled = false;
        }

        string filter;
        void LoadDataFromDB(string filter)
        {
            try
            {
                ordersList = (from e in Globals.Db.View_OrderDetails select e).ToList();
                if (filter != null)
                {
                    ordersList = ordersList.Where(o => o.CustomerName.Contains(filter) ||
                    o.ProductName.Contains(filter)).ToList();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error records:\n" + ex.Message); // FIXME: nicer
            }

            lvOrders.ItemsSource = ordersList;
        }

        private void BtUpdateOrder_Click(object sender, RoutedEventArgs e)
        {
            View_OrderDetails order = (View_OrderDetails)lvOrders.SelectedValue;
            if (order == null)
            {
                return;
            }
            DialogOrderAddUpdate dlgCustomerAddUpdate = new DialogOrderAddUpdate(order);
            dlgCustomerAddUpdate.ShowDialog();
            LoadDataFromDB(filter);
        }

        private void BtAddOrder_Click(object sender, RoutedEventArgs e)
        {
            DialogOrderAddUpdate dlgAddUpdateOrder = new DialogOrderAddUpdate(null);
            dlgAddUpdateOrder.ShowDialog();
            LoadDataFromDB(filter);
        }

        private void LvOrders_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BtUpdateOrder_Click(sender,e);
        }

        View_OrderDetails selectedOrder;
        private void BtDeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            // delete - fetch then schedule for deletion, then save changes
            selectedOrder = (View_OrderDetails)lvOrders.SelectedValue;
            if (selectedOrder == null)
            {
                return;
            }
            try {
                Order orderDel = (from c in Globals.Db.Orders where c.Id == selectedOrder.Id select c).FirstOrDefault<Order>();

                if (orderDel != null)
                {
                    MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this item?\n" + orderDel.Id, "Delete confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.OK)
                    {
                        Globals.Db.Orders.Remove(orderDel); // schedule for deletion from database
                        Globals.Db.SaveChanges();

                        //if chage order people, will  change the orderPeople of tour group 
                        int groupId = orderDel.TourGroupId;
                        TourGroup group = Globals.Db.TourGroups.Where(tg => tg.Id == groupId).FirstOrDefault();
                        int currentNumPeople = group.CurrentOrderedPeople;
                        int newNumPeople = currentNumPeople - orderDel.NumberOfPeople;  //minus the delete number of people in order
                        group.CurrentOrderedPeople = newNumPeople;
                        Globals.Db.SaveChanges();

                        LoadDataFromDB(filter);
                    }
                }
                else
                {
                    Console.WriteLine("record to delete not found");
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error records delete:\n" + ex.Message); 
            }
        }

        private void LvOrders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btUpdateOrder.IsEnabled = true;
            btDeleteOrder.IsEnabled = true;
        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            filter = tbSearch.Text;
            LoadDataFromDB(filter);
        }

        private void BtExport_Click(object sender, RoutedEventArgs e)
        {
            PublicMethod.ExportToCsv<View_OrderDetails>(ordersList);
        }
    }
}
