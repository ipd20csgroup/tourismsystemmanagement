﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for DialogEmployeeAddUpdate.xaml
    /// </summary>
    public partial class DialogEmployeeAddUpdate : Window
    {
        Employee currentEmp;
        public DialogEmployeeAddUpdate(View_Employees emp)
        {
            InitializeComponent();
            if (emp != null)
            {
                currentEmp = (from e in Globals.Db.Employees where e.Id == emp.Id select e).FirstOrDefault<Employee>();
                lblAddUpdateEmployee.Content = "Update Employee";
                btDlgEmployeeAddEdit.Content = "Update";
                lblEmployeeId.Content = emp.Id + "";
                tbDlgEmployeeName.Text = currentEmp.EName;
                tbDlgEmployeeAddress.Text = currentEmp.Address;
                tbDlgEmployeeCity.Text = currentEmp.City;
                tbDlgEmployeeState.Text = currentEmp.State;
                tbDlgEmployeeCountry.Text = currentEmp.Country;
                tbDlgEmployeePostCode.Text = currentEmp.PostCode;
                tbDlgEmployeePhone.Text = currentEmp.Phone;
                tbDlgEmployeeEmail.Text = currentEmp.Email;
                dpDlgEmployeeHireDate.SelectedDate = currentEmp.HireDate;
                tbUserName.Text = currentEmp.Username;
                combRoleLevel.Text = currentEmp.RoleLevel;
            }
            else
            {
                lblAddUpdateEmployee.Content = "Create New Employee";
                btDlgEmployeeAddEdit.Content = "Create";
                combRoleLevel.Text = "Employee";
            }
        }

        private void BtDlgEmployeeAddEdit_Click(object sender, RoutedEventArgs e)
        {
            string name = tbDlgEmployeeName.Text;
            if (name.Length < 1 || name.Length > 50)
            {
                MessageBox.Show(this, "Name must be 1-50.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string address = tbDlgEmployeeAddress.Text;
            if (address.Length < 1 || address.Length > 60)
            {
                MessageBox.Show(this, "Address must be 1-60.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string city = tbDlgEmployeeCity.Text;
            if (city.Length < 1 || city.Length > 15)
            {
                MessageBox.Show(this, "City must be 1-15.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string state = tbDlgEmployeeState.Text;
            if (state.Length < 1 || state.Length > 10)
            {
                MessageBox.Show(this, "Customer state must be 1-10.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string country = tbDlgEmployeeCountry.Text;
            if (country.Length < 1 || country.Length > 15)
            {
                MessageBox.Show(this, "Country must be 1-15.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string postCode = tbDlgEmployeePostCode.Text;
            if (postCode.Length < 1 || postCode.Length > 10)
            {
                MessageBox.Show(this, "PostCode must be 1-10.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string phone = tbDlgEmployeePhone.Text;
            if (phone.Length < 1 || phone.Length > 24)
            {
                MessageBox.Show(this, "Phone must be 1-15.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string email = tbDlgEmployeeEmail.Text;
            if (email.Length < 1 || email.Length > 50)
            {
                MessageBox.Show(this, "Email must be 1-24.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DateTime hireDate = dpDlgEmployeeHireDate.SelectedDate.Value;

            string username = tbUserName.Text;
            if (username.Length < 1 || username.Length > 30)
            {
                MessageBox.Show(this, "username must be 1-30.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string password = pbPassword.Password;
            if ((password.Length < 1 || password.Length > 30) && currentEmp == null) //if update,password field can be empty,not update
            {
                MessageBox.Show(this, "password must be 1-30.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string conFirmPassword = pbConfirmPassword.Password;
            if ((conFirmPassword.Length < 1 || conFirmPassword.Length > 30) && currentEmp == null)
            {
                MessageBox.Show(this, "conFirmPassword must be 1-30.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string roleLevel = combRoleLevel.Text;

            if (roleLevel == null)
            {
                MessageBox.Show(this, "roleLevel must be selected", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (password != conFirmPassword)
            {
                MessageBox.Show(this, "password must be the same as confirm password!", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            password = PublicMethod.MD5Encrypt32(password);  //encryption password

            int len = password.Length;

            if (currentEmp != null)
            {
                try
                {
                    Employee emp = Globals.Db.Employees.Where(c => c.Id == currentEmp.Id).FirstOrDefault();
                    emp.EName = name;
                    emp.Address = address;
                    emp.City = city;
                    emp.State = state;
                    emp.Country = country;
                    emp.PostCode = postCode;
                    emp.Phone = phone;
                    emp.Email = email;
                    emp.HireDate = hireDate;
                    emp.Username = username;
                    if (password != null) { 
                        emp.Password = password;
                    }
                    emp.RoleLevel = roleLevel;
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Error update saving record:\n" + ex.Message);
                }
            }
            else
            {
                try
                {
                    Globals.Db.Employees.Add(new Employee() { EName = name, Address = address, City = city, State = state, Country = country,PostCode = postCode, Phone = phone, Email = email, HireDate = hireDate, Username= username,Password = password,RoleLevel=roleLevel });
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Error add saving record:\n" + ex.Message);
                }
            }
            this.DialogResult = true; // hide dialog
        }

    }
}
