﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for DialogOrderAddUpdate.xaml
    /// </summary>
    public partial class DialogOrderAddUpdate : Window
    {
        Order currentOrder;
        public DialogOrderAddUpdate(View_OrderDetails orderView)
        {
            InitializeComponent();
            getCustomersDataFromDB(customerFilter);
            getAllTourGroupsDataFromDBToListView(groupFilter);
            tbDlgAddUpdateOrderCustomer.IsEnabled = false;
            tbDlgAddUpdateOrderGroup.IsEnabled = false;

            if (orderView != null)
            {
                //var order = (from o in Globals.Db.Orders where o.Id == orderView.Id select o);
                currentOrder = (from o in Globals.Db.Orders where o.Id == orderView.Id select o).FirstOrDefault<Order>();

                tbDlgAddUpdateOrderCustomer.Text = orderView.CustomerName + " (" + orderView.CustomerPhone + ") "; ;
                tbDlgAddUpdateOrderGroup.Text = orderView.ProductName + " (" + orderView.Price + ".00$/Person )";
                tbDlgAddUpdateOrderNumOfPeople.Text = orderView.NumberOfPeople + "";
                comboDlgAddUpdateOrderStatus.Text = orderView.Status;
                pdDlgAddUpdateOrderOrderDate.SelectedDate = orderView.OrderDate;
                btDlgAddUpdateOrderADDDescription.Text = orderView.Description;

                lblAddUpdateOrderTitle.Content = "Update Order";
                btDlgAddUpdateOrderAdd.Content = "Update";

            }
            else
            {
                pdDlgAddUpdateOrderOrderDate.SelectedDate = DateTime.Now.Date;
                comboDlgAddUpdateOrderStatus.Text = "Pending";
                lblAddUpdateOrderTitle.Content = "Create Order";
                btDlgAddUpdateOrderAdd.Content = "Create";
            }
        }
        static List<Customer> customersList = new List<Customer>();
        static List<TourSelectedForOrder> groupsList = new List<TourSelectedForOrder>();
        string customerFilter;
        string groupFilter;
        private void getCustomersDataFromDB(string customerFilter) {
            try
            {
                customersList = (from e in Globals.Db.Customers select e).ToList();
                if (customerFilter != null)
                {
                    customersList = customersList.Where(e => e.Name.Contains(customerFilter) ||
                    e.Phone.Contains(customerFilter)).ToList();
                }
                lvDlgAddUpdateOrderCustomers.ItemsSource = customersList;

            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show("Load Data from Database Error:\n" + ex.Message);
            }

        }

        private void getAllTourGroupsDataFromDBToListView(string groupFilter)
        {
            try
            {
                groupsList = (from tg in Globals.Db.TourGroups
                              join tp in Globals.Db.TravelProducts on tg.TravelProductId equals tp.Id
                              orderby tg.Id
                              where (tg.CurrentOrderedPeople < tp.Maxpeople)  //only show the group current people less than Maxpeople
                              select new TourSelectedForOrder() { TId = tg.Id, Date = tg.DepDate, CurrOrdered = tg.CurrentOrderedPeople, Sts = tg.Status, Name = tp.ProdName, Price = tp.Price }).ToList();
                if (groupFilter != null)
                {
                    groupsList = groupsList.Where(e => e.Name.Contains(groupFilter)).ToList();
                }

                lvDlgAddUpdateOrderToursGroups.ItemsSource = groupsList;
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show("Load Data from Database Error:\n" + ex.Message);
            }
        }

        private void LvDlgAddUpdateOrderCustomers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Customer customerSelected = (Customer)lvDlgAddUpdateOrderCustomers.SelectedValue;
            tbDlgAddUpdateOrderCustomer.Text = customerSelected.Name + " (" + customerSelected.Phone + ") ";
        }

        private void LvDlgAddUpdateOrderToursGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TourSelectedForOrder groupSelected = (TourSelectedForOrder)lvDlgAddUpdateOrderToursGroups.SelectedValue;
            tbDlgAddUpdateOrderGroup.Text = groupSelected.Name + " (" + groupSelected.Price + ".00$/Person )";
        }

        private void BtDlgAddUpdateOrderAdd_Click(object sender, RoutedEventArgs e)
        {
            int customerId;
            Customer customerSelected = (Customer)lvDlgAddUpdateOrderCustomers.SelectedValue;
            int groupId;
            TourSelectedForOrder groupSelected = (TourSelectedForOrder)lvDlgAddUpdateOrderToursGroups.SelectedValue;
            int numOfPeople;
            int.TryParse(tbDlgAddUpdateOrderNumOfPeople.Text, out numOfPeople);
            DateTime orderDate = pdDlgAddUpdateOrderOrderDate.SelectedDate.Value;
            string status = comboDlgAddUpdateOrderStatus.Text;
            OrderStatus orderStatus = (OrderStatus)Enum.Parse(typeof(OrderStatus), status);
            string descrip = btDlgAddUpdateOrderADDDescription.Text;
            int newNumPeople;
            //double rateGoupReady;
            if (currentOrder != null)
            {
                Order order = Globals.Db.Orders.Where(o => o.Id == currentOrder.Id).FirstOrDefault();
                if (customerSelected == null)
                {
                    customerId = order.CustomerId;
                }
                else
                {
                    customerId = customerSelected.Id;
                }

                if (groupSelected == null)
                {
                    groupId = order.TourGroupId;
                }
                else
                {
                    groupId = groupSelected.TId;
                }

                try
                {
                    //if chage order people, will  change the orderPeople of tour group 
                    TourGroup group = Globals.Db.TourGroups.Where(tg => tg.Id == groupId).FirstOrDefault();
                    int currentNumPeople = group.CurrentOrderedPeople;
                    newNumPeople = currentNumPeople - order.NumberOfPeople + numOfPeople;  //minus the old number of people in order,then add new number of people

                    group.CurrentOrderedPeople = newNumPeople;
                    Globals.Db.SaveChanges();

                    order.CustomerId = customerId;
                    order.TourGroupId = groupId;
                    order.NumberOfPeople = numOfPeople;
                    order.OrderDate = orderDate;
                    order.Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), status);
                    order.Description = descrip;
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Error saving record update:\n" + ex.Message);
                }
            }
            else
            {

                if (groupSelected == null)
                {
                    MessageBox.Show(this, "Group must be Selected!", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {
                    groupId = groupSelected.TId;
                }

                if (customerSelected == null)
                {
                    MessageBox.Show(this, "Customer must be Selected!", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {
                    customerId = customerSelected.Id;
                }

                try
                {
                    // if current people equal or more than max people,can not add oreder any more
                    view_TourDetails viewGroup = (from vt in Globals.Db.view_TourDetails where vt.TId == groupId select vt).FirstOrDefault<view_TourDetails>();
                    int currentOrderPeople = viewGroup.CurrOrdered;
                    int maxPeople = viewGroup.MaxPeople;
                    int minPeople = viewGroup.MinPeople;
                    newNumPeople = currentOrderPeople + numOfPeople;
                    if (newNumPeople > maxPeople)
                    {
                        MessageBox.Show(this, "Group already full, please select another group!", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    TourGroup group = Globals.Db.TourGroups.Where(tg => tg.Id == groupId).FirstOrDefault();
                    group.CurrentOrderedPeople = newNumPeople;
                    if (newNumPeople >= minPeople) {
                        group.Status = "Ready";
                    }
                    //double readyRate = (double)newNumPeople / maxPeople;
                    //if (readyRate >= 0.8)
                    //{
                    //    group.Status = "Ready";
                    //}
                    Globals.Db.SaveChanges();

                    Globals.Db.Orders.Add(new Order() { CustomerId = customerId, TourGroupId = groupId, NumberOfPeople = numOfPeople, OrderDate = orderDate, Status = orderStatus, Description = descrip });
                    Globals.Db.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show("Error saving record add:\n" + ex.Message);
                }
            }

            this.DialogResult = true; // hide dialog
        }

        private void TbCustomerSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            customerFilter = tbCustomerSearch.Text;
            getCustomersDataFromDB(customerFilter);
        }

        private void TbEmployeeSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            groupFilter = tbEmployeeSearch.Text;
            getAllTourGroupsDataFromDBToListView(groupFilter);
        }
    }
}
