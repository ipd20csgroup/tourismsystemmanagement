﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DotnetProject
{
    /// <summary>
    /// Interaction logic for EmployeesManagement.xaml
    /// </summary>
    public partial class EmployeesManagement : Page
    {
        static List<View_Employees> employeesList = new List<View_Employees>();
        public EmployeesManagement()
        {
            InitializeComponent();
            LoadDataFromDB(filter);
            btUpdateEmployee.IsEnabled = false;
            btDeleteEmployee.IsEnabled = false;
        }

        string filter;
        void LoadDataFromDB(string filter)
        {
            try
            {
                employeesList = (from e in Globals.Db.View_Employees select e).ToList();
                if (filter != null)
                {
                    employeesList = employeesList.Where(e => e.EName.Contains(filter) ||
                    e.Address.Contains(filter) || e.PostCode.Contains(filter) || e.Phone.Contains(filter)).ToList();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error records:\n" + ex.Message); 
            }
            lvEmployees.ItemsSource = employeesList;
        }

        private void BtUpdateEmployee_Click(object sender, RoutedEventArgs e)
        {
            View_Employees emp = (View_Employees)lvEmployees.SelectedValue;
            if (emp == null)
            {
                return;
            }
            DialogEmployeeAddUpdate dlgCustomerAddUpdate = new DialogEmployeeAddUpdate(emp);
            dlgCustomerAddUpdate.ShowDialog();
            LoadDataFromDB(filter);
        }

        private void BtAddEmployee_Click(object sender, RoutedEventArgs e)
        {
            DialogEmployeeAddUpdate dlgCustomerAddUpdate = new DialogEmployeeAddUpdate(null);
            dlgCustomerAddUpdate.ShowDialog();
            LoadDataFromDB(filter);
        }

        private void LvEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btUpdateEmployee.IsEnabled = true;
            btDeleteEmployee.IsEnabled = true;
        }

        private void LvEmployees_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BtUpdateEmployee_Click(sender, e);
        }

        View_Employees selectedEmployee;
        private void BtDeleteEmployee_Click(object sender, RoutedEventArgs e)
        {
            selectedEmployee = (View_Employees)lvEmployees.SelectedValue;
            if (selectedEmployee == null)
            {
                return;
            }

            Employee employeeDel = (from c in Globals.Db.Employees where c.Id == selectedEmployee.Id select c).FirstOrDefault<Employee>();

            //check if employee has group, cann't be delete
            TourGroup orderByCustomer = (from tg in Globals.Db.TourGroups where tg.TourGuideId == selectedEmployee.Id select tg).FirstOrDefault<TourGroup>();
            if (orderByCustomer != null)
            {
                MessageBox.Show("Employee " + employeeDel.EName + " has tour groups,please delete groups first!", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (employeeDel != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this item?\n" + employeeDel.EName, "Delete confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.OK && employeeDel.Username !="admin")
                {
                    Globals.Db.Employees.Remove(employeeDel); // schedule for deletion from database
                    Globals.Db.SaveChanges();
                    LoadDataFromDB(filter);
                }
            }
            else
            {
                Console.WriteLine("record to delete not found");
            }
        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            filter = tbSearch.Text;
            LoadDataFromDB(filter);
        }
    }
}
